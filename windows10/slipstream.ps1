##--------------------------------------------------------------------------------------------------------------
## Script for slipstreaming drivers to a windows install iso 
##
## Prereq. 	- Download the Windows ADK for windows 10, install only the Deployment tools and Windows PE !
## 		    - Download the latest version of Windows ISO
##		    - Download the drivers to slipstream
##          - Run this script as administrator!
##          - Set the variables before running script
##
## Copyright Bart Michel
## Date: 12-07-2018
##--------------------------------------------------------------------------------------------------------------

## Location of Downloaded files
$IsoLocation = "C:\Temp\Win10_1909_English_x64.iso"
$DriverLocation = "C:\Temp\Drivers"
$Bootorder = "C:\Slipstream\BootOrder.txt"

## Location of directories
$ExportedISOLocation = "C:\Slipstream\WindowsISO"
$ExportedDriverLocation = "C:\Slipstream\Drivers"
$SlipstreamDir = "C:\Slipstream"
$SourcesDir = "C:\Slipstream\WindowsISO\sources"
$MountDir = "C:\Slipstream\Mount"
$oscdimg  = "C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe"
$etfsboot = "C:\Slipstream\WindowsISO\boot\etfsboot.com"
$efisys   = "C:\Slipstream\WindowsISO\efi\microsoft\boot\efisys.bin" 
$new = "C:\Slipstream\FinishedISO\Windows10.iso"
$FinishedISOLocation = "C:\Slipstream\FinishedISO\"
$LogDir = "C:\temp\log.txt"

## Create working dir
MD C:\Slipstream
MD C:\Slipstream\Drivers
MD C:\Slipstream\FinishedISO
MD C:\Slipstream\Mount
MD C:\Slipstream\WindowsISO

## Copy Windows ISO to WindowsISO directory
$mountResult = Mount-DiskImage $IsoLocation -StorageType ISO -PassThru
$driveLetter = ($mountResult | Get-Volume).DriveLetter
$driveletter
$sourceWinISO = $driveletter +":\*"
Copy-Item -Path $sourceWinISO -Destination $ExportedISOLocation -Recurse -Verbose

## Copy Drivers to slipstream Drivers directory
Copy-Item -Path $DriverLocation\* -Destination $ExportedDriverLocation -Recurse -Verbose

## Create Bootorder.txt in slipstream directory
Add-Content $Bootorder "`nboot\bcd`r`nboot\boot.sdi`r`nboot\bootfix.bin`r`nboot\bootsect.exe`r`nboot\etfsboot.com`r`nboot\memtest.exe`r`nboot\en-us\bootsect.exe.mui`r`nboot\fonts\chs_boot.ttf`r`nboot\fonts\cht_boot.ttf`r`nboot\fonts\jpn_boot.ttf`r`nboot\fonts\kor_boot.ttf`r`nboot\fonts\wgl4_boot.ttf`r`nefi\boot\bootx64.efi`r`nefi\microsoft\boot\bcd`r`nefi\microsoft\boot\cdboot.efi`r`nefi\microsoft\boot\cdboot_noprompt.efi`r`nefi\microsoft\boot\efisys.bin`r`nefi\microsoft\boot\efisys_noprompt.bin`r`nefi\microsoft\boot\memtest.efi`r`nefi\microsoft\boot\fonts\chs_boot.ttf`r`nefi\microsoft\boot\fonts\cht_boot.ttf`r`nefi\microsoft\boot\fonts\jpn_boot.ttf`r`nefi\microsoft\boot\fonts\kor_boot.ttf`r`nefi\microsoft\boot\fonts\wgl4_boot.ttf`r`nsources\boot.wim"

## Reomving read-only attribute of Wim files
Set-ItemProperty $SourcesDir\boot.wim -name IsReadOnly -value $false
Set-ItemProperty $SourcesDir\install.wim -name IsReadOnly -value $false

## Inject Drivers in Bootwin file

Dism /Mount-Image /ImageFile:$SourcesDir\boot.wim /index:1 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit 

Dism /Mount-Image /ImageFile:$SourcesDir\boot.wim /index:2 /MountDir:$SlipstreamDir\Mount 
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

## Inject Drivers in Install Wil file
Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:1 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:2 /MountDir:$SlipstreamDir\Mount 
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit 

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:3 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit 

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:4 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:5 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:6 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:7 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:8 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:9 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:10 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

Dism /Mount-Image /ImageFile:$SourcesDir\install.wim /index:11 /MountDir:$SlipstreamDir\Mount
Dism /Image:$SlipstreamDir\Mount /Add-Driver /Driver:$SlipstreamDir\Drivers /Recurse 
Dism /Unmount-Wim /Mountdir:$SlipstreamDir\Mount /Commit

# Create the updated iso.
$data = '2#p0,e,b"{0}"#pEF,e,b"{1}"' -f $etfsboot, $efisys
start-process $oscdimg -args @("-bootdata:$data",'-u2','-m','-o','-udfver102', $ExportedISOLocation, $new) -wait -nonewwindow

## Cleaning up
Dismount-DiskImage -ImagePath $IsoLocation
RD $SlipstreamDir\Drivers -Recurse -Force
RD $SlipstreamDir\WindowsISO -Recurse -Force
RD $SlipstreamDir\Mount -Recurse -Force
del $SlipstreamDir\BootOrder.txt

## Open File explorer ISO Location
ii -Path $FinishedISOLocation

## Console output to log and open
$psise.CurrentPowerShellTab.ConsolePane.Text | Set-Content -Path $LogDir
ii -Path $LogDir


