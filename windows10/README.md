#openstack/windows10

Create OpenStack Windows 10 instance

### Windows 10 - Slipstream

Based on the work from - [Daily Admin Blog](http://dailyadminblog.blogspot.com/2018/07/how-to-slipstream-windows-iso.html) 

Change the following variables
  - $IsoLocation = "C:\Temp\Win10_1909_English_x64.iso"
  - $DriverLocation = "C:\Temp\Drivers"

```sh
./slipstream.ps1
```
### Windows 10 - QEMU - KVM

Create windows10 image for Openstack Flavor m1.medium based on - [Service Engineering (SE) works on cloud computing and service systems](https://blog.zhaw.ch/icclab/windows-image-for-openstack/)
```sh
sudo yum -y install tigervnc
qemu-img create -f qcow2 win10.qcow2 40G
wget wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso
sudo /usr/libexec/qemu-kvm -m 4096 -smp 2 -drive file=Win10.iso,index=1,media=cdrom -drive file=win10.qcow2,if=virtio -drive file=virtio-win.iso,index=2,media=cdrom -net nic,model=virtio -net user -vnc :9  &
sleep 10s
vncviewer localhost:5909 &
```

### Installation 

You might find your mouse cursor is off in the vnc windows.  If so it's time to use the keyboard.

It won't find the drive
![Could not find any drives](screenshots/Could not find any drives.png)

Find the correct driver by chosing "Load Driver"
![Browse for folder](screenshots/Browse for folder.png)

You will have to chose "I don't have internet for now"
![I don't have internet](screenshots/I dont have internet.png)
![Continue with limited setup](screenshots/Continue with limited setup.png)

### Setup

After the installation completes you need to load the network driver
Start "Device Manager"
![Device Manager](screenshots/Could not find any drives.png)

Browse my computer for driver software
![Browse my computer for driver software](screenshots/Browse my computer for driver software.png)

Chose Drive E:
![Search for drivers in this location](screenshots/Search for drivers in this location.png)

Trust the Red Hat, Inc. driver
![Windows security](screenshots/Windows security.png)

Decide if you want windows to finish setting up now you have a network connection
![Let's finish setting up](screenshots/Lets finish setting up.png)

Enable Remote Desktop
![Remote Desktop](screenshots/Remote desktop.png)

Now import the image into OpenStack and launch an instance
