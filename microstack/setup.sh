#!/bin/bash

export CIRROS_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_cirros_image_name"]'`
export UBUNTU_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_ubuntu_image_name"]'`
export CENTOS_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_centos_image_name"]'`

echo "install Ubuntu and CentOS 7 cloud images"
rm ubuntu-18.04-server-cloudimg-amd64.img* 2> /dev/null
rm CentOS-7-x86_64-GenericCloud* 2> /dev/null
wget http://mirrors.servercentral.com/ubuntu-cloud-images/releases/18.04/release/ubuntu-18.04-server-cloudimg-amd64.img
openstack image create --disk-format qcow2 --public --file ubuntu-18.04-server-cloudimg-amd64.img $UBUNTU_IMAGE_NAME
wget https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2.xz 
xz -d CentOS-7-x86_64-GenericCloud.qcow2.xz
openstack image create --disk-format qcow2 --public --file CentOS-7-x86_64-GenericCloud.qcow2 $CENTOS_IMAGE_NAME

#Create region and add access rules
echo "Creating region ${REGION_NAME} and necessary endpoints"
export REGION_NAME=`cat templates/variables.json | jq -r '.["openstack_region_name"]'`
openstack region create $REGION_NAME
openstack endpoint create --region $REGION_NAME identity public http://10.20.20.1:5000/v3/
openstack endpoint create --region $REGION_NAME compute public http://10.20.20.1:8774/v2.1
openstack endpoint create --region $REGION_NAME image public http://10.20.20.1:9292
openstack endpoint create --region $REGION_NAME network public http://10.20.20.1:9696
openstack endpoint create --region $REGION_NAME placement public http://10.20.20.1:8778

echo "retreiving variables from template/variables.json"
export OS_CLOUD=`cat templates/variables.json | jq -r '.["openstack_cloud"]'`
export OS_AUTH_URL=`cat templates/variables.json | jq -r '.["openstack_auth_url"]'`
export OS_REGION_NAME=`cat templates/variables.json | jq -r '.["openstack_region_name"]'`
export OS_PROJECT_NAME=`cat templates/variables.json | jq -r '.["openstack_project_name"]'`
export OS_USER_DOMAIN_NAME=`cat templates/variables.json | jq -r '.["openstack_user_domain_name"]'`
export OS_PROJECT_DOMAIN_NAME=`cat templates/variables.json | jq -r '.["openstack_project_domain_name"]'`
export OS_USERNAME=`cat templates/variables.json | jq -r '.["openstack_username"]'`
export OS_PASSWORD=`cat templates/variables.json | jq -r '.["openstack_password"]'`
export OS_SSH_USERNAME=`cat templates/variables.json | jq -r '.["openstack_ssh_username"]'`
export OS_SSH_KEYFILE=`cat templates/variables.json | jq -r '.["openstack_key_file"]'`
export SECURITY_GROUP_NAME=`cat templates/variables.json | jq -r '.["openstack_security_group_name"]'`
export IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_image_name"]'`
export FLAVOR_NAME=`cat templates/variables.json | jq -r '.["openstack_flavor_name"]'`
export NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_network_name"]'`
export FLOATING_IP_NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_floating_ip_network_name"]'`

echo "Retreiving out local IP"
# These two variables have further environment variables in them
export IP_ADDR=`ip route get 1.1.1.1 | grep -oP 'src \K\S+'`
export OS_AUTH_URL=`echo $OS_AUTH_URL | envsubst`
export OS_SSH_KEYFILE=`echo $OS_SSH_KEYFILE | envsubst`

echo "creating config/clouds.yaml"
envsubst < templates/clouds.yaml > config/clouds.yaml

echo "Retrieving ID's"
export CIRROS_SOURCE_ID=`openstack image show $CIRROS_IMAGE_NAME -f json | jq -r '.["id"]'`
export UBUNTU_SOURCE_ID=`openstack image show $UBUNTU_IMAGE_NAME -f json | jq -r '.["id"]'`
export CENTOS_SOURCE_ID=`openstack image show $CENTOS_IMAGE_NAME -f json | jq -r '.["id"]'`
export FLAVOR_ID=`openstack flavor show $FLAVOR_NAME -f json | jq -r '.["id"]'`
export NETWORK_ID=`openstack network show $NETWORK_NAME -f json | jq -r '.["id"]'`
export FLOATING_IP_NETWORK_ID=`openstack network show $FLOATING_IP_NETWORK_NAME -f json | jq -r '.["id"]'`

echo "Creating config/variables.json"
envsubst < templates/variables.json > config/variables.json

