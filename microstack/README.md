#openstack/microstack

Install and setup microstack on Ubuntu

### Requirements
Ubuntu 18.04 LTS installed on your machine

### Installation
```sh
./install.sh
./setup.sh
```
