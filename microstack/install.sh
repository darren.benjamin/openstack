#!/bin/bash
echo "Install required apps"
sudo apt install jq 
echo "Install microstack"
sudo snap install microstack --classic --beta
echo "Initialize microstack"
sudo microstack.init --auto

echo "Allow instances to connect to internet"
sudo iptables -t nat -A POSTROUTING -s 10.20.20.1/24 ! -d 10.20.20.1/24 -j MASQUERADE
sudo sysctl net.ipv4.ip_forward=1 

echo "alias micrstack.openstack"
sudo snap alias microstack.openstack openstack

