#!/bin/bash

echo "What is your network? (e.g. 10.0.1.0/24)"
read NETWORK
echo "What is your default gateway? (e.g. 10.0.1.254)"
read GATEWAY
echo "First IP we should use for floating IP's (e.g. 10.0.1.201)"
read FIRST_IP
echo "Last IP we should use for floating IP's (e.g. 10.0.1.240)"
read LAST_IP

printf "Network=$NETWORK\nDefault Gateway=$GATEWAY\nFirst IP=$FIRST_IP, Last IP=$LAST_IP\nDoes this look correct?(Y/N)\n"
read ANSWER
if [ ! "Y" == "$ANSWER" ]
then
  echo "Find network-setup details and run setup again"
  exit 1
fi

if [ ! -f /usr/bin/jq ]
then
  echo "Install jq"
  sudo yum -y install jq
fi

USER_ID=`id -u`
GROUP_ID=`id -g`
echo "copying instllation keystonerc_admin to $HOME"
sudo cp ~root/keystonerc_admin ~/keystonerc_admin
echo "copying installation ssh keys to $HOME/.ssh as id_packstack"
mkdir -p ~/.ssh
sudo cp ~root/.ssh/id_rsa ~/.ssh/id_packstack
sudo cp ~root/.ssh/id_rsa.pub ~/.ssh/id_packstack.pub

echo "fixing permissions"
sudo chown $USER_ID:$GROUP_ID ~/.ssh/id_packstack ~/.ssh/id_packstack.pub ~/keystonerc_admin

echo "saving keystonerc_admin to config directory"
cp $HOME/keystonerc_admin config
source config/keystonerc_admin


echo "creating external network"
neutron net-create public --provider:network_type flat --provider:physical_network extnet --router:external
neutron subnet-create --name public_subnet --enable_dhcp=False --allocation-pool=start=$FIRST_IP,end=$LAST_IP --gateway=$GATEWAY public $NETWORK

echo "creating internal network"
neutron net-create private
neutron subnet-create --name private_subnet private 192.168.1.0/24

echo "creating router between two networks"
neutron router-create router1
neutron router-gateway-set router1 public
neutron router-interface-add router1 private_subnet

echo "creating rules to allow ssh and ping"
PROJECT_ID=`openstack project show admin -f json | jq -r '.["id"]'`
SECURITY_GROUP_ID=`openstack security group list --project $PROJECT_ID -f json | jq -r '.[] | select(.Name == "default") | .ID'`
neutron security-group-rule-create --protocol icmp --direction ingress $SECURITY_GROUP_ID
neutron security-group-rule-create --protocol tcp --port-range-min 22 --port-range-max 22 --direction ingress $SECURITY_GROUP_ID

# Create 20 floating IPs
echo "creating 5 floating IPs"
for i in {1..5}
do
  neutron floatingip-create public
done

source config/keystonerc_admin

echo "Install ubuntu cloud image"
# Retrieve names for images
export UBUNTU18_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_ubuntu18_image_name"]'`
export UBUNTU20_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_ubuntu20_image_name"]'`
export CENTOS7_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_centos7_image_name"]'`
export CENTOS8_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_centos8_image_name"]'`

echo "installing images Ubuntu=$UBUNTU18_IMAGE_NAME Ubuntu=$UBUNTU20_IMAGE_NAME CentOS=$CENTOS_IMAGE_NAME"

rm ubuntu-18.04-server-cloudimg-amd64.img* 2> /dev/null
rm ubuntu-20.04-server-cloudimg-amd64.img* 2> /dev/null
rm CentOS-7*.img 2> /dev/null
rm CentOS-8*.img 2> /dev/null
wget http://mirrors.servercentral.com/ubuntu-cloud-images/releases/18.04/release/ubuntu-18.04-server-cloudimg-amd64.img
wget http://mirrors.servercentral.com/ubuntu-cloud-images/releases/20.04/release/ubuntu-20.04-server-cloudimg-amd64.img
openstack image create --disk-format qcow2 --public --file ubuntu-18.04-server-cloudimg-amd64.img "$UBUNTU18_IMAGE_NAME"
openstack image create --disk-format qcow2 --public --file ubuntu-20.04-server-cloudimg-amd64.img "$UBUNTU20_IMAGE_NAME"
wget https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2.xz
wget https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2
xz -d CentOS-7-x86_64-GenericCloud.qcow2.xz
openstack image create --disk-format qcow2 --public --file CentOS-7-x86_64-GenericCloud.qcow2 "$CENTOS7_IMAGE_NAME"
openstack image create --disk-format qcow2 --public --file CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2 "$CENTOS8_IMAGE_NAME"

# For packstack use the one from keystonerc_admin

#Retrieve environment variables
echo "retrieve variables templates/variables.json"
export OS_CLOUD=`cat templates/variables.json | jq -r '.["openstack_cloud"]'`
export SECURITY_GROUP_NAME=`cat templates/variables.json | jq -r '.["openstack_security_group_name"]'`
export IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_image_name"]'`
export FLAVOR_NAME=`cat templates/variables.json | jq -r '.["openstack_flavor_name"]'`
export NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_network_name"]'`
export FLOATING_IP_NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_floating_ip_network_name"]'`
export OPENSTACK_CLOUD=`cat templates/variables.json | jq -r '.["openstack_cloud"]'`

echo "creating config/clouds.yaml"
envsubst < templates/clouds.yaml > config/clouds.yaml

echo "retrieving ID's"
export UBUNTU_SOURCE_ID=`openstack image show "$UBUNTU20_IMAGE_NAME" -f json | jq -r '.["id"]'`
export CENTOS_SOURCE_ID=`openstack image show "$CENTOS8_IMAGE_NAME" -f json | jq -r '.["id"]'`
export FLAVOR_ID=`openstack flavor show $FLAVOR_NAME -f json | jq -r '.["id"]'`
export NETWORK_ID=`openstack network show $NETWORK_NAME -f json | jq -r '.["id"]'`
export FLOATING_IP_NETWORK_ID=`openstack network show $FLOATING_IP_NETWORK_NAME -f json | jq -r '.["id"]'`

echo "creating config/variables.json"
envsubst < templates/variables.json > config/variables.json

echo "Copying to home directory"
cp -r config $HOME/dev_config
