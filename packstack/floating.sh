#!/bin/bash

# Create 20 floating IPs
echo "creating 5 floating IPs"
for i in {1..5}
do
  neutron floatingip-create public
done
