#!/bin/bash
echo "What is the name of your 2nd HDD (e.g. sdb)"
read HDD2

printf "HDD=$HDD2\nDoes this look correct?(Y/N)\n"
read ANSWER
if [ ! "Y" == "$ANSWER" ]
then
  echo "Find 2nd harddisk details and run cinder again"
  exit 1
fi

sudo vgremove cinder-volumes
sudo pvcreate /dev/$HDD2
sudo vgcreate cinder-volumes /dev/$HDD2
