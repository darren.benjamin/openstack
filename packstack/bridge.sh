#!/bin/bash
IP_DEV=`ip route get 1.1.1.1 | grep -oP 'dev \K\S+'`
if [ "" == $IP_DEV ]
then
  echo "Could not determine the ethernet interface"
  exit 1
fi
MAC_ADDR=`ip link show $IP_DEV | awk '/ether/ {print $2}'`
if [ "" == $MAC_ADDR ]
then
  echo "Could not determine the mac address of the default interface"
  exit 1
fi
cp templates/ifcfg-br-ex config/ifcfg-br-ex
cp templates/ifcfg-int config/ifcfg-int
echo Creating bridge network br-ex using MAC_ADRR=$MAC_ADDR and dev=$IP_DEV
sed -i "s/^MACADDR.*/MACADDR=$MAC_ADDR/g" config/ifcfg-br-ex
sed -i "s/^OVSDHCPINTERFACES.*/OVSDHCPINTERFACES=$IP_DEV/g" config/ifcfg-br-ex
sed -i "s/^DEVICE=.*/DEVICE=$IP_DEV/g" config/ifcfg-int
sudo cp config/ifcfg-br-ex /etc/sysconfig/network-scripts/ifcfg-br-ex
sudo cp config/ifcfg-int /etc/sysconfig/network-scripts/ifcfg-$IP_DEV
sudo ifdown $IP_DEV; sudo ifup $IP_DEV
