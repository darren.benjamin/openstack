#!/bin/bash

if [ ! -f /usr/bin/jq ]
then
  echo "Install jq"
  sudo yum -y install jq
fi

USER_ID=`id -u`
GROUP_ID=`id -g`
echo "copying instllation keystonerc_admin to $HOME"
sudo cp ~root/keystonerc_admin ~/keystonerc_admin
echo "copying installation ssh keys to $HOME/.ssh as id_packstack"
mkdir -p ~/.ssh
sudo cp ~root/.ssh/id_rsa ~/.ssh/id_packstack
sudo cp ~root/.ssh/id_rsa.pub ~/.ssh/id_packstack.pub

echo "fixing permissions"
sudo chown $USER_ID:$GROUP_ID ~/.ssh/id_packstack ~/.ssh/id_packstack.pub ~/keystonerc_admin

echo "saving keystonerc_admin to config directory"
cp $HOME/keystonerc_admin config
source config/keystonerc_admin

source config/keystonerc_admin

#Retrieve environment variables
echo "retrieve variables templates/variables.json"
export UBUNTU18_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_ubuntu18_image_name"]'`
export UBUNTU20_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_ubuntu20_image_name"]'`
export CENTOS_IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_centos_image_name"]'`
export OS_CLOUD=`cat templates/variables.json | jq -r '.["openstack_cloud"]'`
export SECURITY_GROUP_NAME=`cat templates/variables.json | jq -r '.["openstack_security_group_name"]'`
export IMAGE_NAME=`cat templates/variables.json | jq -r '.["openstack_image_name"]'`
export FLAVOR_NAME=`cat templates/variables.json | jq -r '.["openstack_flavor_name"]'`
export NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_network_name"]'`
export FLOATING_IP_NETWORK_NAME=`cat templates/variables.json | jq -r '.["openstack_floating_ip_network_name"]'`
export OPENSTACK_CLOUD=`cat templates/variables.json | jq -r '.["openstack_cloud"]'`

echo "creating config/clouds.yaml"
envsubst < templates/clouds.yaml > config/clouds.yaml

echo "retrieving ID's"
export UBUNTU_SOURCE_ID=`openstack image show "$UBUNTU20_IMAGE_NAME" -f json | jq -r '.["id"]'`
export CENTOS_SOURCE_ID=`openstack image show "$CENTOS_IMAGE_NAME" -f json | jq -r '.["id"]'`
export FLAVOR_ID=`openstack flavor show $FLAVOR_NAME -f json | jq -r '.["id"]'`
export NETWORK_ID=`openstack network show $NETWORK_NAME -f json | jq -r '.["id"]'`
export FLOATING_IP_NETWORK_ID=`openstack network show $FLOATING_IP_NETWORK_NAME -f json | jq -r '.["id"]'`

echo "creating config/variables.json"
envsubst < templates/variables.json > config/variables.json

echo "Copying to home directory"
cp -r config $HOME/dev_config
