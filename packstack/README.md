#openstack/packstack

Install and setup packstack using your existing network

### Requirements
CentOS 7 or 8 installed on Bare metal or Virtual machine

Note: 
  - If you have a 2nd HDD you can use it for cinder
  - You must know a range of IPs on your current LAN you can use for floating IPs

### Installation CentOS 7
```
./install_centos7.sh
./bridge.sh
./setup.sh
```

### Installation CentOS 8
```
./install_centos8.sh
./bridge.sh
./setup.sh
```

Optionally if you have a second drive you can use it as a cinder volume
```
./cinder.sh
```

Once done remember to reboot
```
sudo reboot
```

### Extra reading

- [Using your own network](https://www.rdoproject.org/networking/neutron-with-existing-external-network/)
- [Cloning the MAC on that network](https://www.youtube.com/watch?v=8zFQG5mKwPk)
- [Using Virtual Ethernet Adapters in Promiscuous Mode on a Linux Host](https://kb.vmware.com/s/article/287)
