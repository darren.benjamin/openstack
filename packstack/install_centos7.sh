echo "disabling firewall, removing NetworkManager, and enabling network"
sudo systemctl disable firewalld
sudo systemctl stop firewalld
sudo systemctl disable NetworkManager
sudo systemctl stop NetworkManager
sudo systemctl enable network
sudo systemctl start network

IP_ADDR=`ip route get 1.1.1.1 | grep -oP 'src \K\S+'`
if [ "" == $IP_ADDR ]
then
  echo "Could not determine ip address interface"
  exit 1
fi
IP_DEV=`ip route get 1.1.1.1 | grep -oP 'dev \K\S+'`
if [ "" == $IP_DEV ]
then
  echo "Could not determine ethernet interface"
  exit 1
fi
HOSTNAME_FULL=`hostname`
HOSTNAME=`hostname -s`
echo "adding host entry for $IP_ADDR addr $HOSTNAME"
if [ "$HOSTNAME" == "$HOSTNAME_FULL" ]
then
  echo $IP_ADDR $HOSTNAME | sudo tee -a /etc/hosts
else
  echo $IP_ADDR $HOSTNAME_FULL $HOSTNAME | sudo tee -a /etc/hosts
fi


echo "installating packages"
sudo yum update -y
sudo yum install -y centos-release-openstack-stein
sudo yum-config-manager --enable openstack-stein
sudo yum update -y
sudo yum install -y openstack-packstack
sudo yum install -y ntp
sudo systemctl enable ntpd
sudo systemctl start ntpd

echo "installing packstack and briding to $IP_DEV"
sudo packstack --allinone --provision-demo=n --os-neutron-ovs-bridge-mappings=extnet:br-ex --os-neutron-ovs-bridge-interfaces=br-ex:$IP_DEV --os-neutron-ml2-type-drivers=vxlan,flat
